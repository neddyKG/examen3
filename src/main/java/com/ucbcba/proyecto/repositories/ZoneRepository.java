package com.ucbcba.proyecto.repositories;

import com.ucbcba.proyecto.entities.Restaurant;
import com.ucbcba.proyecto.entities.Zone;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface ZoneRepository extends JpaRepository<Zone, Integer> {
}
