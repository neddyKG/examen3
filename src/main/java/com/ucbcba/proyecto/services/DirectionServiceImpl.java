package com.ucbcba.proyecto.services;


import com.ucbcba.proyecto.entities.Direction;
import com.ucbcba.proyecto.repositories.DirectionRepository;
import com.ucbcba.proyecto.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class DirectionServiceImpl implements DirectionService{

    private DirectionRepository directionRepository;
    @Autowired
    @Qualifier(value = "directionRepository")
    public void setProductRepository(DirectionRepository directionRepository) {
        this.directionRepository = directionRepository;
    }

    @Override
    public Iterable<Direction> listAllDirection() {
        return directionRepository.findAll();
    }

    @Override
    public Direction getDirectionById(Integer id) {
        return directionRepository.findOne(id);
    }

    @Override
    public Direction saveDirection(Direction direction) {
        return directionRepository.save(direction);
    }

    @Override
    public void deleteDirection(Integer id) {
        directionRepository.delete(id);
    }
}
