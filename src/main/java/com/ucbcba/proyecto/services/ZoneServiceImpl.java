package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.Zone;
import com.ucbcba.proyecto.repositories.ZoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class ZoneServiceImpl implements ZoneService{
    private ZoneRepository zoneRepository;

    @Autowired
    @Qualifier(value = "zoneRepository")
    public void setZoneRepository(ZoneRepository zoneRepository) {
        this.zoneRepository = zoneRepository;
    }
    @Override
    public Iterable<Zone> listAllZones( ) {
        return zoneRepository.findAll();
    }

    @Override
    public Zone getZoneById(Integer id) {
        return zoneRepository.findOne(id);
    }

    @Override
    public Zone saveZone(Zone zone) {
        return zoneRepository.save(zone);
    }

    @Override
    public void deleteZone(Integer id) {
        zoneRepository.delete(id);
    }
}
